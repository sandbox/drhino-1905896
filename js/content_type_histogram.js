(function($) { 
  function loadTables() {

    var labels = Drupal.settings.content_type_histogram.header;
    var counts_by_content_type = Drupal.settings.content_type_histogram.rows;

    for (var content_type in counts_by_content_type) {
      var count = counts_by_content_type[content_type];


      var lineChartData = {
        labels : labels,
        datasets : [
          {
            fillColor : "rgba(220,220,220,0.5)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            data: count
          }
        ]
      }

      var steps = 5;
      var max = Math.max.apply(Math, count);
      if (max < 5) { max = 5; } // If all points are 0 the graph does not render properly
      var canvasName = "cth_canvas_" + content_type;
      var myLine = new Chart(document.getElementById(canvasName).getContext("2d")).Line(
        lineChartData, 
        { 
          scaleOverride : true,
          scaleStepWidth : Math.ceil(max / steps),
          scaleStartValue : 0,
          scaleSteps : steps
      });
    }
  }

  Drupal.behaviors.content_type_histogram = {
    attach:function() {
      loadTables();
    }
  }
}(jQuery));