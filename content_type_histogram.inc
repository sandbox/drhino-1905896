<?php

function content_type_histogram_data($from, $to, $type) {
  $query = "SELECT COUNT(*) AS count
    FROM node
    WHERE YEARWEEK(FROM_UNIXTIME(created)) BETWEEN YEARWEEK(FROM_UNIXTIME(:from)) AND YEARWEEK(FROM_UNIXTIME(:to))
    AND type = :type
    GROUP BY YEARWEEK(FROM_UNIXTIME(created))";

  return (int) db_query($query, array(':from' => $from, ':to' => $to, ':type' => $type ))->fetchAll();
}

function content_type_histogram_beginning() {
  $types = node_type_get_names();
  $query = "SELECT MIN(created) FROM node WHERE type IN (:types)";
  return (int) db_query($query, array(':types' => $types))->fetchField();
}
